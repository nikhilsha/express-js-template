# Express JS Template

A simple template for Express JS in Node.js. The purpose of this repo is to make it so you don't have to remake the entire environment.

## What to do

Install your desired node packages. Static files such as JS, CSS, and other resources will be stored in the <em>static</em> directory. The sub-folders in the <em>static</em> directory are optional and don't have to be used. Replace the ocurrences of express-js-template with the name of your project.
