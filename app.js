"use strict";

const express = require('express');
const app = express()
const port = 3000;

app.set('view engine', 'ejs');
app.set('views', `${__dirname}/views`);

app.use(express.static("static"));

app.get('/', (req, res) => {
    res.render("helloWorld");
});

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});